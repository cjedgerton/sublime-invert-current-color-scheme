# Sublime Text - Invert Current Color Scheme Package
Simple plugin to invert the current color scheme. For those who prefer to use inverted colors in their main OS settings, but still want to use a normal color scheme in Sublime.

Invert the current scheme from the command palette by fuzzy searching and executing `Invert Current Color Scheme: Invert`.

